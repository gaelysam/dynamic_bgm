put music files in a folder named music, or anywhere except a folder named "sounds"


registration
=========================

```lua
dbgm.register_music({

        file = "/path/to/file",
        name = "",
        length = 180, 
        author = "",
        license = "",
        special = false, -- use to exclude music from randomly chosen music, use if you want to implement a special usecase with the api.
        min_pos = {x=num,y=num,z=num},
        max_pos = {x=num,y=num,z=num},
        near_nodes = {}, -- a list of nodes that must be near the player, accepts groups
        near_nodes_radius = 10,
        min_light = 0,
        max_light = minetest.LIGHT_MAX,
        custom_check = function(p_name) --return true to allow possibility of playing when randomly chosen, false otherwise
                
})

```

required
---------------
file

        the absolute path to the file, use minetest.get_modpath and append the music folder and soundfile

name

        the name of the file without any file extention


length

        the approximate length of the music, must be greater than or equal to the actual length 

optional
--------------

author = "",

        in later versions the info will be displayed

license = "",

        in later versions the info will be displayed or available in-game anyhow

special = false, -- use to exclude music from randomly chosen music, use if you want to implement a special usecase with the api.

        special sounds cannot be chosen randomly, to be used for an expansion of the api

min_pos = {x=num,y=num,z=num},

        use this to set the postional range that the sound is available to be randomly chosen in. Default is 
        {x = -32000,y = -32000,z = -32000}

max_pos = {x=num,y=num,z=num},


        use this to set the postional range that the sound is available to be randomly chosen in. Default is 
        {x = 32000,y = 32000,z = 32000}


near_nodes = {}, 

        a list of nodes that must be near the player for the sound to be chosen . accepts groups

near_nodes_radius = 10,

        the radius to check for the above nodes. default 10

min_light = 0,

        the min light needed for the sound to be played (daylight affects)

max_light = minetest.LIGHT_MAX,

        the max light needed for the sound to be played (daylight affects)

custom_check = function(p_name) --return true to allow possibility of playing when randomly chosen, false otherwise

        by default simple returns true, use this to implement custom checks to determine if the sound can be considersed. Please keep this lightweight, as this is called once for each player, every interval




