dbgm.music = {}
dbgm.handles = {}
dbgm.queued_music = {}
dbgm.counter = 0

-- TODO: spread the per player calculation out over server ticks

-- dbgm.register_music({
--         file = "/path/to/file",
--         name = "",
--         length = 180, --length of music in sec
--         author = "",
--         license = "",
--         special = false, -- use to exclude music from randomly chosen music, use if you want to implement a special usecase with the api.
--         min_pos = {x=num,y=num,z=num},
--         max_pos = {x=num,y=num,z=num},
--         near_nodes = {}, -- a list of nodes that must be near the player, accepts groups
--         near_nodes_radius = 10,
--         min_light = 0,
--         max_light = minetest.LIGHT_MAX,
--         custom_check = function(p_name) --return true to allow possibility of playing when randomly chosen, false otherwise
-- })


function dbgm.register_music(def)
        if dbgm.debug then
                minetest.log("registering music:"..def.name)
        end
        if not def.file then
                minetest.log("error","[!] DBGM music file not defined!")
                dbgm.get_traceback()
                return
        end
        if not def.name then
                minetest.log("error","[!] DBGM music name not defined!")
                dbgm.get_traceback()
                return
        end
        local default_custom_check = function(p_name) return true end

        if not def.custom_check then def.custom_check = default_custom_check end

        dbgm.music[def.name] = def
end

function dbgm.get_traceback()
        if debug then
                minetest.log("traceback: ".. debug.traceback())
        end
end

function dbgm.pos_is_near_nodes(pos,radius,nodes)
        if dbgm.debug then
                minetest.log("pos_is_near_nodes was called")
        end
        if nodes == nil then return true end
        if not(type(nodes) == "table") then
                minetest.log("error","[!] DBGM nodes is not a table!")
                dbgm.get_traceback()
                return true 
        end
        if minetest.find_node_near(pos, radius, nodes, true) then
                return true
        end
        return false
end
                        

function dbgm.pos_is_within_bounds(pos,min_pos,max_pos)
        if dbgm.debug then
                minetest.log("pos is within bounds was called")
        end
        local minp,maxp = vector.sort(min_pos, max_pos)
        if pos.x > minp.x and pos.y > minp.y and pos.z > minp.z and pos.x < maxp.x and pos.y < maxp.y and pos.z < maxp.z then
                return true
        end
        return false
end 


function dbgm.pos_is_within_light_bounds(pos,min_light,max_light)
        if dbgm.debug then
                minetest.log("pos_is_within_lightbounds was called")
        end
        if minetest.get_node_light(pos) and 
        minetest.get_node_light(pos) >= min_light and minetest.get_node_light(pos) <= max_light then
                return true
        end
        return false
end


function dbgm.get_available_music(p_name)
        if dbgm.debug then
                minetest.log("get_available_music was called")
        end
        if not p_name or not minetest.get_player_by_name(p_name) then
                minetest.log("error","[!] DBGM: player doesnt exist")
                dbgm.get_traceback()
        end
        local player = minetest.get_player_by_name(p_name)
        local pos = player:get_pos()
        local available_music = {}
        for music_name, music_def in pairs(dbgm.music) do 
                if not(music_def.special) then
                        if music_def.custom_check(p_name) then
                                local min_pos = music_def.min_pos or {x = -32000,y = -32000,z = -32000}
                                local max_pos = music_def.max_pos or {x = 32000,y = 32000,z = 32000}
                                local min_light = music_def.min_light or 0
                                local max_light = music_def.max_light or minetest.LIGHT_MAX
                                local radius = music_def.near_nodes_radius or 10
                                local near = dbgm.pos_is_near_nodes(pos,radius,music_def.near_nodes)
                                local in_light_range =  dbgm.pos_is_within_light_bounds(pos,min_light,max_light)
                                if dbgm.debug then
                                        minetest.log("near is:".. tostring(near))
                                        minetest.log("in_light_range  is:".. tostring(in_light_range))
                                end
                                if dbgm.pos_is_within_bounds(pos,min_pos,max_pos) and near and in_light_range then
                                        table.insert(available_music,music_def)
                                end
                        end
                end
        end
        if dbgm.debug then
                minetest.log("available_music:\n"..dump(available_music))
        end
        return available_music
end


function dbgm.clear_handle_after(seconds,p_name,handle)

        if dbgm.debug then
                minetest.log("clear_handle_after was called")
        end

        minetest.after(seconds,function(p_name,handle)
                if handle and dbgm.handles[p_name] and dbgm.handles[p_name] == handle then
                        dbgm.handles[p_name] = nil
                end
        end,p_name,handle)

end


function dbgm.fade_music(p_name)
        if dbgm.debug then
                minetest.log("fade_music was called")
        end
        if dbgm.handles[p_name] then
                minetest.sound_fade(dbgm.handles[p_name], .5, 0)
                local handle = dbgm.handles[p_name]
                dbgm.clear_handle_after(2,p_name,handle)
        end
end


function dbgm.fade_and_then_play_music(p_name)
        if dbgm.debug then
                minetest.log("fade_and_then_play_music was called")
        end
        local time = 0

        if dbgm.handles[p_name] then
                dbgm.fade_music(p_name)
                time = 3
        end
        minetest.after(time,function(p_name)

                dbgm.handles[ p_name ] = minetest.sound_play({
                        name = dbgm.queued_music[p_name], 
                        gain = 1.0, 
                        pitch = 1.0},{
                        to_player = p_name,
                        gain = 1.0,   -- default
                        fade = 0.0,   -- default, change to a value > 0 to fade the sound in
                        pitch = 1.0,  -- default
                } )
                local handle = dbgm.handles[p_name]
                dbgm.clear_handle_after(dbgm.music[dbgm.queued_music[p_name]].length,p_name,handle)
                --music is being played, it is no longer queued
                dbgm.queued_music[p_name] = nil

        end,p_name)
end




-- minetest.dynamic_add_media(options, callback)

--     options: table containing the following parameters
--         filepath: path to a media file on the filesystem
--         to_player: name of the player the media should be sent to instead of all players (optional)
--         ephemeral: boolean that marks the media as ephemeral, it will not be cached on the client (optional, default false)
--     callback: function with arguments name, which is a player name
--     Pushes the specified media file to client(s). (details below) The file must be a supported image, sound or model format. Dynamically added media is not persisted between server restarts.
--     Returns false on error, true if the request was accepted
--     The given callback will be called for every player as soon as the media is available on the client.
--     Details/Notes:
--     If ephemeral=false and to_player is unset the file is added to the media sent to clients on startup, this means the media will appear even on old clients if they rejoin the server.
--     If ephemeral=false the file must not be modified, deleted, moved or renamed after calling this function.
--     Regardless of any use of ephemeral, adding media files with the same name twice is not possible/guaranteed to work. An exception to this is the use of to_player to send the same, already existent file to multiple chosen players.
--     Clients will attempt to fetch files added this way via remote media, this can make transfer of bigger files painless (if set up). Nevertheless it is advised not to use dynamic media for big media files.




function dbgm.send_music_to_player(p_name,music_name,path)
        if dbgm.debug then
                minetest.log("send_music_to_player was called")
        end

        local player = minetest.get_player_by_name(p_name)
        if not player then
                minetest.log("error","[!] DBGM: player doesnt exist")
                dbgm.get_traceback()
        end
        dbgm.queued_music[p_name] = music_name
        minetest.dynamic_add_media({filepath = path,to_player = p_name, ephemeral = dbgm.ephemeral,}, dbgm.fade_and_then_play_music )
                
end



function dbgm.globalstep(dtime)
        dbgm.counter = dbgm.counter + dtime
        if dbgm.counter < dbgm.interval then
                return
        end
        if dbgm.debug then
                minetest.log("globalstep ticked out")
                --minetest.dynamic_add_media({filepath = "/home/user/.minetest/mods/dynamic_bgm/woodland_music/music",to_player = "MisterE", ephemeral = true,},function(p_name) minetest.chat_send_all(p_name) return end )
        end
        dbgm.counter = 0
        for _, player in pairs(minetest.get_connected_players()) do
                local p_name = player:get_player_name()
                if dbgm.player_settings[p_name].music_on then
                        if p_name then
                                local available_music = dbgm.get_available_music(p_name)
                                if available_music and #available_music > 0 then
                                        math.randomseed(os.time())
                                        local chosen_music = available_music[math.random(1,#available_music)]
                                        dbgm.send_music_to_player(p_name,chosen_music.name,chosen_music.file)
                                end
                        end
                end
        end
end





minetest.register_globalstep(function(dtime)

        dbgm.globalstep(dtime)

end)